var Hotshots 	= require('hot-shots');
var exec		= require('child_process').exec;

var datadog;

function DatadogEvents( sDatadogServer ) {
	datadog = new Hotshots( { host: sDatadogServer, errorHandler : console.error } );
	datadog.socket.on( 'error', console.error );
}

exports = module.exports = DatadogEvents;

var fExecuteGitCommand = ( sCommand, sDirectory ) => {
	return new Promise( function( resolve, reject ) {
		exec( sCommand
			, { cwd : sDirectory }
			, function ( error, stdout, stderr ) {
				if( error ) {
					reject( error );
				}
				resolve( stdout.trim() );
			} );
	} );
};


var fWriteEvent = ( sSystem, sEnvironment, sTag, sCommit ) => {
	return new Promise( function( resolve, reject ) {
		datadog.event( 'Release ' + sTag + ' to ' + sEnvironment + ' ' + sSystem
					 , 'Commit: ' + sCommit
					 , { priority : 'low' 
					   , source_type_name : 'Release'
					   }
					 , [ 'System:' + sSystem, 'Environment:' + sEnvironment, 'Tag:' + sTag ]
					 , ( err ) => {
					 		if( err ) {
					 			reject( err );
					 			return;
					 		} 
					 		datadog.close( ( err ) => { 
					 			if( err ) {
					 				reject( err );
					 				return;
					 			} 
					 			resolve();
					 		} );
						} );
	} );
};

//DatadogEvents-0-00-003-1-gbfff6d9 => DatadogEvents-0-00-003
//DatadogEvents-0-00-003 => DatadogEvents-0-00-003
//DatadogEvents-0-00-003-B1-1-1-gbfff6d9 => DatadogEvents-0-00-003-B1-1
//DatadogEvents-0-00-003-B1-1 => DatadogEvents-0-00-003-B1-1
var fParseTag = ( sRawTag ) => {
	return sRawTag.replace( /\-\d+\-g\w{7}/, '' );
};

DatadogEvents.prototype.SendReleaseEvent = ( sSystem, sEnvironment, sProjectDirectory ) => {
	return new Promise( ( resolve, reject  ) => {
		var sTag;
		fExecuteGitCommand( 'git describe --tags', sProjectDirectory )
			.then( ( sTagResult ) => {
					sTag = fParseTag( sTagResult );
					return fExecuteGitCommand( 'git rev-parse HEAD', sProjectDirectory );
				} )
			.then( ( sCommit ) => {
					return fWriteEvent( sSystem, sEnvironment, sTag, sCommit );
				} )
			.then( resolve )
			.catch( reject );
	} );
};